import sys
import numpy as np

class State:
    
    def __init__(self, board):
        self.board = board
    
    @classmethod
    def from_string(cls, string):
        board = string.splitlines()
        for i, row in enumerate(board):
            board[i] = list(row)
        board = np.array(board)

        return cls(board)    
    
    
    def get_transitions(self):
        """ Return (action, state) pairs that are one move away from current state. """
        cars = self._get_cars()
        height, width = self.board.shape

        transitions = []
        for color, rect in cars.items():
            top, bot, left, right = rect.bounds

            # Vertically moving car
            if left == right:
                if top > 0 and self._is_empty(rect.moved(-1, 0), color):
                    transitions.append((color+'-up', self._moved(rect, -1, 0)))
                if bot < height-1 and self._is_empty(rect.moved(1, 0), color):
                    transitions.append((color+'-down', self._moved(rect, 1, 0)))

            # Horizontally moving car
            elif top == bot:
                if left > 0 and self._is_empty(rect.moved(0, -1), color):
                    transitions.append((color+'-left', self._moved(rect, 0, -1)))
                if right < width-1 and self._is_empty(rect.moved(0, 1), color):
                    transitions.append((color+'-right', self._moved(rect, 0, 1)))

        return transitions
    
    def _get_cars(self):
        """ Parse board and return cars in rectangle format. """
        cars = {}
        for y, row in enumerate(self.board):
            for x, char in enumerate(row):
                if char != '.':
                    lst = cars.get(char, [])
                    lst.append((y,x))
                    cars[char] = lst
        for color, squares in cars.items():
            cars[color] = Rectangle.from_squares(squares)
        return cars
    
    def _is_empty(self, rect, color):
        """ Check whether the given car can occupy the given rectangle without
        driving into any of the other cars. """
        top, bot, left, right = rect.bounds
        crop = self.board[top:bot+1, left:right+1]
        return np.all(np.logical_or(crop == '.', crop == color))
    
    def _moved(self, rect, dy, dx):
        """ Return a copy of the board with a car moved. """
        t, b, l, r = rect.bounds

        board = self.board.copy()
        board[t:b+1, l:r+1] = '.'
        board[t+dy:b+dy+1, l+dx:r+dx+1] = self.board[t:b+1, l:r+1]

        return State(board)
    
    def __hash__(self):
        return hash(self.board.tobytes())
    
    def __eq__(self, other):
        return np.array_equal(self.board, other.board)

class Rectangle:

    def __init__(self, top, bot, left, right):
        self.bounds = (top, bot, left, right)

    @classmethod
    def from_squares(cls, squares):
        """ Convert a list of squares into a rectangle instance. """
        y, x = squares[0]
        top = bot = y
        left = right = x

        for y, x in squares[1:]:
            top = min(y, top)
            bot = max(y, bot)
            left = min(x, left)
            right = max(x, right)
        
        return cls(top, bot, left, right)
    
    def moved(self, yoffset, xoffset):
        """ Return a copy of self offseted by the given values. """
        top, bot, left, right = self.bounds
        return Rectangle(top+yoffset, bot+yoffset, left+xoffset, right+xoffset)
    
    def __repr__(self):
        return 'Rectangle({} {} {} {})'.format(*self.bounds)


class Path:
    def __init__(self, state, actions=[]):
        self.tail = state
        self.actions = []


if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        start = State.from_string(f.read())
    
    # Do breadth-first search
    front = {start} # States to expand on next iteration
    visited = set() # Expanded states
    paths = dict() # List of moves leading to expanded or 'front' states
    final = None
    while final is None:

        new_front = set()
        for state in front:

            transitions = state.get_transitions()
            for action, next_state in transitions:
                if next_state in visited:
                    continue
                paths[next_state] = paths.get(state, []) + [action]
                new_front.add(next_state)
                if next_state.board[2, 5] == 'r':
                    final = next_state
            
            visited.add(state)
        front = new_front
        # print('Front: {}, visited: {}, paths: {}'.format(len(front), len(visited), len(paths)))
    
    print(paths[final])
