local utils = require("utils")

local Module = {}

function Module.parseBoard(text)  
  local table = {}
  for line in string.gmatch(text, "[^\r\n]+") do
    local row = {}
    for c in line:gmatch"." do
      row[#row + 1] = c
    end
    table[#table + 1] = row
  end

  local cars = {}
  for rowi=1, #table do
    local row = table[rowi]
    for coli=1, #row do
      local color = row[coli]
      if color ~= '.' then
        if cars[color] == nil then
          cars[color] = {{rowi, coli}}
        else
          local squares = cars[color]
          squares[#squares+1] = {rowi, coli}
        end
      end
    end
  end

  for color, squares in pairs(cars) do
    local ymin, ymax, xmin, xmax
    for i=1, #squares do
      local y, x = unpack(squares[i])
      if ymin == nil or ymin > y then ymin = y end
      if ymax == nil or ymax < y then ymax = y end
      if xmin == nil or xmin > x then xmin = x end
      if xmax == nil or xmax < x then xmax = x end
    end
    local ori
    if ymin == ymax then ori = 'hor' else ori = 'ver' end
    cars[color] = {y=ymin, x=xmin, orientation=ori, size=ymax-ymin+xmax-xmin+1}
  end

  return cars
end

function Module.parseSolution(text)
  local moves = {}
  for word in string.gmatch(text, "'(.-)'") do
    color, dir = string.match(word, "(.)-(.+)")
    if dir == "up" then
      dir = {y=-1, x=0}
    elseif dir == "down" then
      dir = {y=1, x=0}
    elseif dir == "left" then
      dir = {y=0, x=-1}
    elseif dir == "right" then
      dir = {y=0, x=1}
    end
    moves[#moves + 1] = {color=color, x=dir.x, y=dir.y}
  end

  return moves
end

-- Load the problem setup and solution at the given path.
-- Assume solution has the same name except with '_sol' appended.
-- If not found, try searching in the 'layouts' directory.
function Module.loadExternal(path)
  local dir, name, ext = utils.splitPath(path)
  local probname = {path, dir .. "layouts/" .. name .. ext}
  local f = io.open(probname[1])
  probname.using = probname[1]

  if f == nil then
    f = io.open(probname[2])
    probname.using = probname[2]
  end

  local cars, moves, info = nil, {}, nil
  if f == nil then
    info = "File " .. probname[1] .. " not found...\n"
  else
    local text = f:read("*all")
    f:close()
    cars = Module.parseBoard(text)
    info = "Loaded input " .. probname.using .. "\n"

    dir, name, ext = utils.splitPath(Prompt)
    local solname = {dir .. name .. "_sol" .. ext,
                     dir .. "layouts/" .. name .. "_sol" .. ext}

    f = io.open(solname[1])
    solname.using = solname[1]

    if f == nil then
      f = io.open(solname[2])
      solname.using = solname[2]
    end

    if f == nil then
      info = info .. "Solution " .. solname[1] .. " not found...\n"
    else
      text = f:read("*all")
      f:close()
      moves = Module.parseSolution(text)
      info = info .. "Loaded solution " .. solname.using .. "\n"
    end

    return cars, moves, info, true
  end
  return nil, nil, info, false
end

return Module