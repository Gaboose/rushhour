TileW, TileH = 72,72

function love.conf(t)
  t.window.width = TileW*10
  t.window.height = TileH*6
  t.window.resizable = false
  t.modules.joystick = false
end