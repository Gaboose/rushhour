local utf8 = require("utf8")
local utils = require("utils")
local loaders = require("loaders")

State = {}
function State.init(self)
  self.cars = {}
  self.moves = {}
  self.moveIndex = 0
end

function State.moveForth(self)
  if self.moveIndex == #self.moves then return end
  self.moveIndex = self.moveIndex + 1
  local move = self.moves[self.moveIndex]
  local car = self.cars[move.color]
  car.x = car.x + move.x
  car.y = car.y + move.y
end

function State.moveBack(self)
  if self.moveIndex == 0 then return end
  local move = self.moves[self.moveIndex]
  local car = self.cars[move.color]
  car.x = car.x - move.x
  car.y = car.y - move.y
  self.moveIndex = self.moveIndex - 1
end

function love.load()

  love.window.setTitle("Rush Hour")

  local font = love.graphics.newFont("Inconsolata-Regular.ttf", 14)
  love.graphics.setFont(font)
  love.keyboard.setKeyRepeat(true)

  local text = love.filesystem.read("input.txt")
  State:init()
  State.cars = loaders.parseBoard(text)

  text = love.filesystem.read("input_sol.txt")
  State.moves = loaders.parseSolution(text)
  
  Tileset = love.graphics.newImage('tileset.png')
  local tilesetW, tilesetH = Tileset:getWidth(), Tileset:getHeight()
  
  Quads = {}
  for i, xy in ipairs{{0,0},{0,1},{0,2},{1,0},{1,1},{1,2},{2,0},{2,1},{2,2}} do
    Quads[i] = love.graphics.newQuad((5+xy[1])*TileW, xy[2]*TileH, TileW, TileH, tilesetW, tilesetH)
  end

  CarQuads = {
    ver2=love.graphics.newQuad(0,       0,     TileW,   TileH*2, tilesetW, tilesetH),
    ver3=love.graphics.newQuad(TileW,   0,     TileW,   TileH*3, tilesetW, tilesetH),
    hor2=love.graphics.newQuad(TileW*2, 0,     TileW*2, TileH,   tilesetW, tilesetH),
    hor3=love.graphics.newQuad(TileW*2, TileH, TileW*3, TileH,   tilesetW, tilesetH)
  }

  RedTileset = utils.paintRed(Tileset)
  
  TileTable = {
     { 1,4,4,4,4,7 },
     { 2,5,5,5,5,8 },
     { 2,5,5,5,5,5 },
     { 2,5,5,5,5,8 },
     { 2,5,5,5,5,8 },
     { 3,6,6,6,6,9 },
  }

  Prompt = ""
  Info = ""
end

function love.keypressed(key)
  if key == "right" then
    State:moveForth()
  elseif key == "left" then
    State:moveBack()
  elseif key == "backspace" then
    local byteoffset = utf8.offset(Prompt, -1)
    if byteoffset then
        Prompt = string.sub(Prompt, 1, byteoffset-1)
    end
  elseif key == "return" then
    local cars, moves, info, ok = loaders.loadExternal(Prompt)
    if ok then
      State:init()
      State.cars = cars
      State.moves = moves
      Prompt = ""
    end
    Info = info
  end
end

function love.textinput(t)
    Prompt = Prompt .. t
end

function love.draw()
  love.graphics.setColor(255, 255, 255)

  -- Background
  for rowIndex=1, #TileTable do
    local row = TileTable[rowIndex]
    for columnIndex=1, #row do
      local number = row[columnIndex]
      local x = (columnIndex-1)*TileW
      local y = (rowIndex-1)*TileH
      love.graphics.draw(Tileset, Quads[number], x, y)
    end
  end

  -- Cars
  for color, car in pairs(State.cars) do
    local x = (car.x-1)*TileW
    local y = (car.y-1)*TileH
    local tileset = Tileset
    if color == 'r' then
      tileset = RedTileset
    end
    love.graphics.draw(tileset, CarQuads[car.orientation .. car.size], x, y)
  end
  
  -- Terminal
  local pad = 10
  local x = TileW*6 + pad
  local w = love.graphics.getWidth() - x - pad
  love.graphics.printf("> " .. Prompt, x, pad, w)

  love.graphics.setColor(255, 255, 0)
  love.graphics.printf(Info, x, 40, w)

  love.graphics.setColor(255, 255, 255)
  love.graphics.printf("Use arrow keys to move along the solution.\n\n" ..
                       "Try typing in a filename to load a different problem setup.\n",
                       x, love.graphics.getHeight() - 85, w)
end
