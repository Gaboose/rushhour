local Module = {}

function Module.splitPath(path)
  local dir, ext = "", ""
  i = path:find("%.[^%.]*$")
  if i ~= nil then
    ext = path:sub(i)
    path = path:sub(1,i-1)
  end
  j = path:find("/[^/]*$")
  k = path:find("\\[^\\]*$")
  if k ~= nil and j ~= nil and k > j then j = k end
  if j ~= nil then
    dir = path:sub(1, j)
    path = path:sub(j+1)
  end
  return dir, path, ext
end

function Module.paintRed(image)
  local w, h = image:getWidth(), image:getHeight()
  newimg = love.image.newImageData(w, h)
  newimg:paste(image:getData(), 0, 0, 0, 0, w, h)
  newimg:mapPixel(function(x,y,r,g,b,a)
    r = math.min(r*2, 255)
    g = math.max(g-(255-g)/2, 0)
    b = math.max(b-(255-b)/2, 0)
    return r,g,b,a
  end)
  return love.graphics.newImage(newimg)
end

return Module