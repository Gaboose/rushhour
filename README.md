# Rush Hour Traffic Grid Solver #

Here lies:

* a Python script that solves Rush Hour ([the board game](https://en.wikipedia.org/wiki/Rush_Hour_(board_game))) challenge cards
* a [Love](https://love2d.org/) app that visualizes solutions

## Run Solver ##

```bash
$ git clone git@bitbucket.org:Gaboose/rushhour.git
$ cd rushhour
$ python rushhour.py layouts/input.txt
['B-left', 'B-left', 'A-left', 'C-left', 'A-left', 'C-left', 'E-up', 'E-up', 'F-up', 'F-up', 'H-right', 'H-right', 'I-up', 'J-left', 'J-left', 'J-left', 'I-down', 'H-left', 'F-down', 'H-left', 'E-down', 'r-right', 'E-down', 'r-right', 'F-down', 'E-down', 'r-right', 'F-down', 'r-right']
```

To save the solution so it can be opened with the visualizer, run

```bash
$ python rushhour.py layouts/input.txt > layouts/input_sol.txt
```

You don't have to do this for the challenges that are already in the repository. Solutions for those are already precomputed.

## Run Visualizer ##

![shot-2017-01-20_21-13-55.jpg](https://bitbucket.org/repo/kodjjk/images/4028780108-shot-2017-01-20_21-13-55.jpg)

### Windows ###

* Download [RushHour-win32.zip](https://bitbucket.org/Gaboose/rushhour/downloads/RushHour-win32.zip)
* Start RushHour/RushHour.exe

### Linux ###

Unfortunately there are no self-contained binaries for Linux.

Install love 0.10.2 on your Linux distribution.

#### On Ubuntu ####

```bash
$ add-apt-repository ppa:bartbes/love-stable
$ apt-get update
$ apt-get install love=0.10.2ppa1 # Version name may lose the 'ppa1' part in the future
```

#### On Arch Linux ####

At the time of writing the [arch love package](https://www.archlinux.org/packages/community/i686/love/) is at version 0.10.2-1, so you can just run

```bash
$ pacman -S love
```

After that, `cd` to your cloned git repo and run

```bash
$ love loveapp
```

### Once It's Running ###

Use the left and right arrow keys to move through the solution. You can load one of three challenges in the 'layouts' directory by typing `input.txt`, `hard.txt` or `93-moves.txt` and hitting enter.